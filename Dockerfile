FROM zzrot/alpine-caddy
MAINTAINER Alternatif Bilisim
# docker build -t altbil/bildirge .
# docker run -d -p 80:80 altbil/bildirge
RUN apk --no-cache add openssl
RUN wget https://github.com/AlternatifBilisim/aletetme.org/archive/master.zip -O /tmp/master.zip
RUN unzip /tmp/master.zip -d /tmp
RUN mv /tmp/aletetme.org-master /var/www/html
RUN rm -rf /tmp/master.zip
